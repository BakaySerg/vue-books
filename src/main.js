import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueResource from "vue-resource";
import LineChart from './components/LineChart.js';

// Vue.config.productionTip = false;
Vue.use(VueResource);

// Vue.http.options.root = 'http://localhost:8080/'

Vue.directive('linechart', LineChart)

export const globalEmitter = new Vue();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");