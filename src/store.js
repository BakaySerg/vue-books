import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // books: [],
    // labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    // data: [40, 39, 12, 40, 39, 76, 40],
  },
  // mutations: {
  //   addBooks(state, payload) {
  //     // console.log('mutatation');
  //     // console.log(payload);
  //     state.books.push(payload);
  //  }
  // },
  // actions: {
  //   loadBooks({commit}, payload){
  //     this.$http
  //     .get("http://fakerestapi.azurewebsites.net/api/Books")
  //     .then(response => {
  //       return response.json();
  //     })
  //     .then(incomingBooks => {
  //       return this.books = incomingBooks
  //     });
  //     commit('addBooks', payload.incomingBooks)
  //   }
  // },
  // getters: {
  //   getBooks(state) {
  //     return state.books
  //   }
  // },
});