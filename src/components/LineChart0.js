import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins


export default {
  extends: Line,  //Bar
  mixins: [reactiveProp],
  props: {
    chartData: {
      type: Object,
      default: null
    },
    options: {
      type: Object,
      default: null
    }
  },
  data() {
    return {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      data:  [25, 36, 20, 32, 17, 12, 40],

      // chartData: {
      //   labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      //   data:  [40, 39, 12, 40, 39, 76, 40],
      // },
    }
  },
  mounted () {
    this.$http
    .get("http://fakerestapi.azurewebsites.net/api/Books")
    .then(response => {
      return response.json();
    })
    .then(books => {
      this.books = books;
      // eslint-disable-next-line no-console
      // console.log(books);
    });

    this.renderChart({
      labels: this.labels,
      datasets: [
        {
          label: 'Page Count',
          borderColor: '#41b883',
          borderWidth: 4,
          fill:false,
          data: this.data,
        }
      ]
    }, {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        position: "bottom"
      },
      layout: {
        padding: {
            left: 10,
            right: 10,
            top: 15
        }
      }
    })
  },


}