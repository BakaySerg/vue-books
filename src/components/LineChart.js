import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins
import { globalEmitter } from '../main';

export default {
  extends: Line,
  mixins: [reactiveProp],   //reactiveData
  props: {
    chartData: {
      type: Object,
      default: null
    },
    options: {
      type: Object,
      default: null
    },
    chartName: ''
  },
  mounted () {
    this.createChart();
  },
  methods: {
    createChart(){
      this.renderChart(this.chartData, this.options)
    },

  },
  created() {
    globalEmitter.$on('createChart', (data) => {
      // eslint-disable-next-line no-console
      console.log(data);
    })
  }
}